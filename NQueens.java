/**
 * Author: Daniel Jackson
 * Class: CS345
 * Assignment: Lab 2, N-Queens problem
 */

import java.util.Scanner;

public class NQueens {
	public static int[] Q;
	public static Scanner scan;
	public static int step = 0;

	private static Boolean validPosition(int k) {
		
		for (int i = 0; i < k; i++) {
			// If two queens are on the same row or same diagonal
			if ((Q[i] == Q[k]) || (Math.abs(Q[i] - Q[k]) == Math.abs(i - k))) {
				return false;
			}
		}
		return true;
	}

	public static void placeQueens(int N) {
		String solution = "";
		Q = null;
		Q = new int[N];
		int k = 0;

		while (k < N) {

			while ((k < N) && (!validPosition(k))) {
				
				Q[k] = Q[k] + 1;
			}

			if ((k == N - 1) && (Q[k] < N)) {
				solution += "The solution is: \n[";
				k = 0;
			
				while (k < N) {
					solution += Q[k];

					if (k < N - 1) {
						solution += ", ";
					} else {
						solution += "]";
					}
					k++;
				}
				System.out.println(solution);
				return;
			} else if ((k < N - 1) && (Q[k] < N)) {
				k = k + 1;
				Q[k] = 0;
			} else {
				// the positions of the first k Queens cannot possibly lead
				// to a solution. So we must backtrack.
				
				k = k - 1;
				if (k < 0) {
					System.out.println("There is no solution possible!");
					return;
				} else {
					Q[k] = Q[k] + 1;
					
				}
			}
		}
	}

	public static void main(String[] args) {
		int s = 0;
		int finish = 1;
		scan = new Scanner(System.in);

		while (finish != 0) {
			System.out.println("Please enter a number between 1-30");
			s = scan.nextInt();

			if (s < 0) {
				System.out.println("That is an invalid input");
			} else if (s > 30) {
				System.out.println("That is an invalid input");
			} else {
				placeQueens(s);
				System.out.println(step);
			}
			System.out.println("continue? y(1) n(0)");
			finish = scan.nextInt();
		}

	}
}